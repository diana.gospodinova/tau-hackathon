package pages;

import com.google.common.io.Files;
import org.openqa.selenium.*;
import pages.base.BaseElement;
import pages.elements.Button;
import pages.elements.InputTextField;
import pages.elements.TextField;
import utils.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static utils.Utils.getKey;

public class Page extends BaseElement {
    private WebDriver driver;
    private Button button;
    private TextField textField;
    private InputTextField inputTextField;

    public Page() {
        this.driver = Utils.getWebDriver();
        this.button = new Button();
        this.textField = new TextField();
        this.inputTextField = new InputTextField();
    }

    public void openPage(String page) {
        driver.get(Utils.getElementsMappingProperties()
                .getProperty(page));
    }

    public String getPage() {
        return driver.getCurrentUrl();
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public String getElementText(String identifier) {
        String key = getKey(identifier);
        String[] keys = key.split("\\.");
        String text = "";

        switch (keys[1]) {
            case "id":
                text = textField.getTextById(Utils.getElementsMappingProperties().getProperty(key));
                break;
            case "class":
                text = textField.getTextByClass(Utils.getElementsMappingProperties().getProperty(key));
                break;
            case "xpath":
                text = textField.getTextByXpath(Utils.getElementsMappingProperties().getProperty(key));
                break;
            default:
                throw new NoSuchElementException(key + " doesn't exist");
        }

        return text;
    }

    public List<String> getElementsText(String identifier) {
        String key = getKey(identifier);
        List<String> collection = new ArrayList<>();
        boolean elementExists = true;
        int counter = 1;

        while (elementExists) {
            try {
                collection.add(textField.getTextByXpath
                        ("(" + Utils.getElementsMappingProperties().getProperty(key) + ")[" + counter + "]"));
                counter++;
            } catch (NoSuchElementException ex) {
                elementExists = false;
            }
        }

        return collection;
    }

    public void clickButton(String identifier) {
        String key = getKey(identifier);
        String[] keys = key.split("\\.");
        switch (keys[1]) {
            case "id":
                button.clickById(Utils.getElementsMappingProperties().getProperty(key));
                break;
            case "xpath":
                button.clickByXpath(Utils.getElementsMappingProperties().getProperty(key));
                break;
            default:
                throw new NoSuchElementException(key + " doesn't exist");
        }
    }

    public void enterValueInTextField(String identifier, String value) {
        String key = getKey(identifier);
        String[] keys = key.split("\\.");

        switch (keys[1]) {
            case "id":
                inputTextField.enterValueById(Utils.getElementsMappingProperties().getProperty(key), value);
                break;
            case "xpath":
                inputTextField.enterValueByXPath(Utils.getElementsMappingProperties().getProperty(key), value);
                break;
            default:
                throw new NoSuchElementException(key + " doesn't exist");
        }
    }

    public boolean elementExists(String element) {
        String key = getKey(element);
        String[] keys = key.split("\\.");
        List<WebElement> elements;

        switch (keys[1]) {
            case "id":
                elements = findElementsById(Utils.getElementsMappingProperties().getProperty(key));
                break;
            case "xpath":
                elements = findElementsByXpath(Utils.getElementsMappingProperties().getProperty(key));
                break;
            default:
                throw new NoSuchElementException(key + " doesn't exist");
        }

        return !elements.isEmpty();
    }

    public String getElementAttributeText(String element, String attribute) {
        String key = getKey(element);
        String[] keys = key.split("\\.");
        String text = "";

        switch (keys[1]) {
            case "id":
                text = findElementById(Utils.getElementsMappingProperties().getProperty(key)).getAttribute(attribute);
                break;
            case "xpath":
                text = findElementByXpath(Utils.getElementsMappingProperties().getProperty(key)).getAttribute(attribute);
                break;
            default:
                throw new NoSuchElementException(key + " doesn't exist");
        }

        return text;
    }

    public String getElementLabel(String element) {
        String key = getKey(element);
        String[] keys = key.split("\\.");
        String label = "";

        switch (keys[1]) {
            case "id":
                label = findElementById(Utils.getElementsMappingProperties().getProperty(key)).getText();
                break;
            case "xpath":
                label = findElementByXpath(Utils.getElementsMappingProperties().getProperty(key)).getText();
                break;
            default:
                throw new NoSuchElementException(key + " doesn't exist");
        }
        return label;
    }

    public boolean verifyChart(String fileName, String element) throws IOException {
        String key = getKey(element);
        return compareCharts(fileName, findElementById(Utils.getElementsMappingProperties().getProperty(key)));
    }


    // common method to compare the baseline image against actual whole page / specific element
    private boolean compareCharts(String fileName, WebElement element) throws IOException {
        TakesScreenshot camera = (TakesScreenshot) driver;
        File screenshot = camera.getScreenshotAs(OutputType.FILE);
        System.out.println("Screenshot taken: " + screenshot.getAbsolutePath());
        try {
            Files.move(screenshot, new File("resources/result/" + fileName));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        /*try {
            // take buffer data from botm image files //
            BufferedImage biA = ImageIO.read(new File("resources/snap/" + fileName));
            DataBuffer dbA = biA.getData().getDataBuffer();
            int sizeA = dbA.getSize();
            BufferedImage biB = ImageIO.read(new File("resources/result/" + fileName));
            DataBuffer dbB = biB.getData().getDataBuffer();
            int sizeB = dbB.getSize();
            // compare data-buffer objects //
            if (sizeA == sizeB) {
                for (int i = 0; i < sizeA; i++) {
                    if (dbA.getElem(i) != dbB.getElem(i)) {
                       return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Failed to compare image files ...");
            return false;
        }*/
        BufferedImage imgA = null;
        BufferedImage imgB = null;

        try {
            File fileA = new File("resources/result/" + fileName);
            File fileB = new File("resources/snap/" + fileName);

            imgA = ImageIO.read(fileA);
            imgB = ImageIO.read(fileB);
        } catch (IOException e) {
            System.out.println(e);
        }

        if (imgA == null || imgB == null) {
            return false;
        }

        int width1 = imgA.getWidth();
        int width2 = imgB.getWidth();
        int height1 = imgA.getHeight();
        int height2 = imgB.getHeight();
        double percentage = 0;

        if ((width1 != width2) || (height1 != height2)) {
            System.out.println("Error: Images dimensions" +
                    " mismatch");
            return false;
        } else {
            long difference = 0;
            for (int y = 0; y < height1; y++) {
                for (int x = 0; x < width1; x++) {
                    int rgbA = imgA.getRGB(x, y);
                    int rgbB = imgB.getRGB(x, y);
                    int redA = (rgbA >> 16) & 0xff;
                    int greenA = (rgbA >> 8) & 0xff;
                    int blueA = (rgbA) & 0xff;
                    int redB = (rgbB >> 16) & 0xff;
                    int greenB = (rgbB >> 8) & 0xff;
                    int blueB = (rgbB) & 0xff;
                    difference += Math.abs(redA - redB);
                    difference += Math.abs(greenA - greenB);
                    difference += Math.abs(blueA - blueB);
                }

            }

            // Total number of red pixels = width * height
            // Total number of blue pixels = width * height
            // Total number of green pixels = width * height
            // So total number of pixels = width * height * 3
            double total_pixels = width1 * height1 * 3;

            // Normalizing the value of different pixels
            // for accuracy(average pixels per color
            // component)
            double avg_different_pixels = difference /
                    total_pixels;

            // There are 255 values of pixels in total
            percentage = (avg_different_pixels /
                    255) * 100;

            System.out.println("Difference Percentage-->" +
                    percentage);
        }

        if (percentage < 3) {
            return true;
        } else {
            return false;
        }
    }

}


